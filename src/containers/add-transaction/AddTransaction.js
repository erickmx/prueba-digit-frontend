import React, { Component } from "react";
import moment from "moment";
import { DatePicker, Input, Button } from "../../components";
import "./index.css";
import { inject, observer } from "mobx-react";

@inject("balance", "category", "expense")
@observer
class AddTransation extends Component {
  state = {
    transactionDay: moment(),
    amount: 0,
    description: ""
  };

  handleChangeInput = ev => {
    this.setState({ amount: ev.target.value });
  };

  handleClickButton = async () => {
    const { amount, transactionDay, description } = this.state;
    await this.props.expense.create({
      Created: transactionDay,
      amount,
      description
    });
  };

  handleDate = date => {
    this.setState({
      transactionDay: date
    });
  };

  render() {
    return (
      <div className="transaction__container">
        <DatePicker
          handleDate={this.handleDate}
          date={this.state.transactionDay}
        />
        <Input
          label="Ingrese el monto gastado"
          type="number"
          onChange={this.handleChangeInput}
          value={this.state.amount}
        />
        <Button style={{ marginTop: "3rem" }} onClick={this.handleClickButton}>
          Agregar
        </Button>
      </div>
    );
  }
}

export { AddTransation };
