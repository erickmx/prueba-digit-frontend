import React, { Component } from "react";
import "./App.css";
import { AddTransation } from "./containers";

import { library } from "@fortawesome/fontawesome-svg-core";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStroopwafel } from "@fortawesome/free-solid-svg-icons";
import stores from "./stores";
import { Provider } from "mobx-react";

library.add(faStroopwafel);

class App extends Component {
  render() {
    return (
      <Provider {...stores}>
        <div className="App">
          <AddTransation />
        </div>
      </Provider>
    );
  }
}

export default App;
