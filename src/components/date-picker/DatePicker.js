import React, { Component } from "react";
import Picker from "react-datepicker";
import "./index.css";

import "react-datepicker/dist/react-datepicker.css";

class DatePicker extends Component {
  handleChange = date => {
    this.props.handleDate(date);
  };

  render() {
    return (
      <Picker
        selected={this.props.date}
        onChange={date => this.handleChange(date)}
        selectsStart={this.props.date}
      />
    );
  }
}

export default DatePicker;
