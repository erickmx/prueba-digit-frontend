import React from "react";
import "./style.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Button = ({
  onClick,
  type,
  label,
  className = "",
  icon = "",
  children,
  ...props
}) => (
  <button
    className={`button ${className}`}
    onClick={ev => onClick(ev)}
    type={type}
    {...props}
  >
    <FontAwesomeIcon icon="plus" /> {children}
  </button>
);

export default Button;
