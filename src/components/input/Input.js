import React from "react";
import "./style.css";

const Input = ({ onChange, type, label, className, ...props }) => (
  <div className={`input__container ${className || ""}`}>
    <label className="input__label"> {label} </label>
    <input
      className="input__field"
      onChange={ev => onChange(ev)}
      type={type}
      {...props}
    />
  </div>
);

export default Input;
