import { action, observable } from "mobx";
import { api } from "../utils";

class ExpenseStore {
  @observable
  expenses = [];
  @observable
  expense = {};

  @action
  getExpenses = () => {
    try {
      const { data } = api.get("/Expenses");
      this.expenses = data;
    } catch (error) {
      console.error("ERROR_GET_EXPENSES", error);
    }
  };

  @action
  findById = async id => {
    try {
      const { data } = api.get(`/Expenses/${id}`);
      this.expense = data;
    } catch (error) {
      console.error("ERROR_FIND_EXPENSE", error);
    }
  };

  @action
  create = transaction => {
    try {
      const { data } = api.post("/Expenses", transaction);
      this.expenses.push(data);
    } catch (error) {
      console.error("ERROR_CREATE_EXPENSE", error);
    }
  };

  @action
  update = (id, transaction) => {
    try {
      const { data } = api.patch(`/Expenses/${id}`, transaction);
      const index = this.expenses.findIndex(expense => id == expense.id);
      this.expenses[index] = data;
    } catch (error) {
      console.error("ERROR_UPDATE_EXPENSE", error);
    }
  };
}
export { ExpenseStore };
