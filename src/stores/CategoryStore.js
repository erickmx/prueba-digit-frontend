import { action, observable } from "mobx";
import { api } from "../utils";

class CategorieStore {
  @observable
  categoryes = [];
  @observable
  category = {};

  @action
  getCategoryes = async () => {
    try {
      const { data } = await api.get("/Categories");
      this.categoryes = data;
    } catch (error) {
      console.error("ERROR_GET_CATEGORIES", error);
    }
  };

  @action
  findById = async id => {
    try {
      const { data } = await api.get(`/Categories/${id}`);
      this.category = data;
    } catch (error) {
      console.error("ERROR_FIND_CATEGORY", error);
    }
  };

  @action
  create = async category => {
    try {
      const { data } = await api.post("/Categories", category);
      this.categoryes.push(data);
    } catch (error) {
      console.error("ERROR_CREATE_CATEGORY", error);
    }
  };

  @action
  update = async (id, category) => {
    try {
      const { data } = await api.patch(`/Categories/${id}`, category);
      const index = this.categoryes.findIndex(cat => id == cat.id);
      this.categoryes[index] = data;
    } catch (error) {
      console.error("ERROR_UPDATE_CATEGORY", error);
    }
  };
}
export { CategorieStore };
