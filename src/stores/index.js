import { BalanceStore } from "./BalanceStore";
import { CategoryStore } from "./CategoryStore";
import { ExpenseStore } from "./ExpenseStore";

export default {
  balance: new BalanceStore(),
  category: new CategoryStore(),
  expense: new ExpenseStore()
};
