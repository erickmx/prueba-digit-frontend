import { action, observable } from "mobx";
import { api } from "../utils";

class BalanceStore {
  @observable
  balances = [];
  @observable
  balance = {};

  @action
  getBalances = async () => {
    try {
      const { data } = await api.get("/Balances");
      this.balances = data;
    } catch (error) {
      console.error("ERROR_GET_BALANCE", error);
    }
  };

  @action
  findById = async id => {
    try {
      const { data } = await api.get(`/Balances/${id}`);
      this.balance = data;
    } catch (error) {
      console.error("ERROR_FIND_BALANCE", error);
    }
  };

  @action
  create = async amount => {
    try {
      const { data } = await api.post("/Balances", { money: amount });
      this.balances.push(data);
    } catch (error) {
      console.error("ERROR_CREATE_BALANCE", error);
    }
  };

  @action
  update = async (id, amount) => {
    try {
      const { data } = await api.patch(`/Balances/${id}`, { money: amount });
      const index = this.balances.findIndex(balance => id == balance.id);
      this.balances[index] = data;
    } catch (error) {
      console.error("ERROR_UPDATE_BALANCE", error);
    }
  };
}
export { BalanceStore };
